// Load the SDK and UUID
var AWS = require('aws-sdk');
var uuid = require('uuid');
const adminPool  = 'ap-south-1_QdbxpotNp'
const userPool  ='ap-south-1_im8WXNuUK'
const pool = adminPool
const poolData={UserPoolId:pool}
AWS.config.update({region: 'ap-south-1'});
var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

function convertToAWSPromise(request) {
  const result = request.promise();
  return result;
}
exports.listAllUsers = function() {

  var params = {
      UserPoolId: pool, /* required */
      AttributesToGet: [],
     
  
    };
    cognitoidentityserviceprovider.listUsers(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
    });
  
}

exports.adminCreateUser = function() {
  var params = {
    UserPoolId: pool, /* required */
    Username: 'hks@drluotan.com', /* required */
    

    ForceAliasCreation:  false,
    // MessageAction: RESEND | SUPPRESS,
    TemporaryPassword: 'Temp@1234567',
    UserAttributes: [
      {
        Name: 'email', /* required */
        Value: 'hks@drluotan.com',
      },
      {
        Name: 'custom:role_id', /* required */
        Value: 'sample_role',
      },
      /* more items */
    ],
    
  };
  cognitoidentityserviceprovider.adminCreateUser(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}



exports.adminUpdateUser = function() {
  var params = {
    UserPoolId: pool, /* required */
    Username: 'sample01@drluotan.com', /* required */
  };
  var params = {
    UserAttributes: [
      {
        Name: 'custom:role_id', /* required */
        Value: 'sample_role2',
      },
      /* more items */
    ],
    UserPoolId: pool, /* required */
    Username: 'sample01@drluotan.com', /* required */
  };
  cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}
exports.admincreateGroup = function() {
  var params = {
    GroupName: 'sample_group_01', /* required */
    UserPoolId: pool, /* required */
  };
  cognitoidentityserviceprovider.createGroup(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}

const addUsertoGroup=  function (
  username,
  groupName,
) {

    var params = {
      GroupName: groupName /* required */,
      UserPoolId: pool /* required */,
      Username: username /* required */,
    };
    return convertToAWSPromise(
    cognitoidentityserviceprovider.adminAddUserToGroup(
      params
    ))

};

exports.assignRoleBasedGroups = async function(roles = [], username) {
  const addToGroups = roles.map((role) =>
    addUsertoGroup(username, role)
  );
    const res =await Promise.allSettled(addToGroups);
    const erroredRequests = res.filter(item=>item.status==="failed")
    console.log(erroredRequests)
    if (erroredRequests.length) {
      console.log("error")
      throw { res , erroredRequests };
    }
    return res
}



exports.adminListGroupsForUser =  async function(username){
  var params = {
    UserPoolId: pool, /* required */
    Username: username||'sample01@drluotan.com', /* required */
  };
  const request = cognitoidentityserviceprovider.adminListGroupsForUser(params)
  const groups = await convertToAWSPromise(request)
  console.log(groups)
  
}

exports.listGroups= async function(roles) {
  const params = {
    UserPoolId: pool /* required */,
  };
  const res  = await  convertToAWSPromise(cognitoidentityserviceprovider.listGroups(params))
  console.log(res)
}

exports.validateRoles = async function validateRoles(roles) {
  try {
    const isValidRoles  = true
    const params = {
      UserPoolId: pool /* required */,
    };
    const { Groups = [] } = await convertToAWSPromise(
      cognitoidentityserviceprovider.listGroups(params)
    );
    roles.forEach((role) => {
      const group = Groups.find((group) => group.GroupName === role);
      
      if (!group) {
        throw new Error("Group not found")
      }
      console.log("found group for" , role)
    });
    return isValidRoles;
  } catch (e) {
    return false;
  }
}

function removeUserFromGroup(username, groupName) {
  var params = {
    GroupName: groupName /* required */,
    UserPoolId: poolData.UserPoolId /* required */,
    Username: username /* required */,
  };
  return convertToAWSPromise(
    cognitoidentityserviceprovider.adminRemoveUserFromGroup(params)
  );
}

exports.removeUserRoleBasedGroups= async function (roles = [], username) {
  const removeGroups = roles.map((role) => removeUserFromGroup(username, role));
  const res = await Promise.allSettled(removeGroups);
  const convertedRequests = res.map((item,i) => ({
    ...item,
    groupName: roles[i],
  }));
  const erroredRequests = convertedRequests.filter(
    (item, i) => item.status === 'rejected'
  );
  if (erroredRequests.length) {
    throw { message: 'remove user falied for Groups Listed ', erroredRequests };
  }
  return convertedRequests;
}